��    4      �  G   \      x     y     �  
   �     �     �     �     �     �     �  	   �     �  	             #     :  
   N  (   Y     �     �     �     �     �     �     �     �  	   �     �     �     �       &   	     0     9     <     H     U     ^  	   d     n     w     |  	   �     �  )   �  6   �     �      	      *     K     P  ,   W  �   �  0   s     �     �     �     �     �     �     �     �     
	  "   	  
   =	  &   H	  -   o	     �	     �	  5   �	     
     
     
     $
     1
  	   K
     U
  
   [
     f
     x
     �
     �
     �
  4   �
     �
     �
     �
     �
     
  	     	   "     ,     ;     C     L     ^  2   f  <   �  *   �  %     %   '     M     V  D   ^     3   #          (                  )   +   
      1                             '             2   ,   4   0   	               *                              $       &              /             .      %      !          "                  -               '&1' toolbar configuration Action Attach tab Border Button1 Cancel Close Close all tabs Close other tabs Close tab Close tabs on the right Configure Configure &1 toolbar Configure main toolbar Configure shortcuts Detach tab Do you really want to reset the toolbar? Expander Export Export shortcuts Find shortcut Gambas shortcuts files Go back Help Huge Icon size Import Import shortcuts Large Medium Multiple document interface management Next tab OK Orientation Previous tab Reparent Reset Separator Shortcut Show Small Sort tabs Space This file is not a Gambas shortcuts file. This shortcut is already used by the following action: Toolbar configuration Unable to export shortcut files. Unable to import shortcut files. Undo Window You are going back to the default shortcuts. Project-Id-Version: gb.form.mdi 3.8.90
PO-Revision-Date: 2015-09-20 17:52 UTC
Last-Translator: Jesus <ea7dfh@users.sourceforge.net>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Configuración de la barra de herramientas '&1'  Acción Anclar pestaña Borde - Cancelar Cerrar Cerrar todas las pestañas Cerrar las otras pestañas Cerrar pestaña Cerrar las pestañas de la derecha Configurar Configurar la barra de herramientas &1 Configurar la barra de herramientas principal Configurar accesos directos Desanclar pestaña ¿Realmente desea restaurar la barra de herramientas? Expansor Exportar Exportar atajos Buscar atajo Archivos de atajos Gambas Ir atrás Ayuda Muy grande Tamaño del icono Importar Importar atajos Grande Mediano Interfaz de administración de múltiples documentos Pestaña siguiente OK Orientación Pestaña anterior Cambiar padre Reiniciar Separador Acceso directo Mostrar Pequeño Ordenar pestañas Espacio Este archivo no es un archivo de atajos de Gambas. Este acceso directo ya está usado por la siguiente acción: Configuración de la barra de herramientas Imposible exportar archivo de atajos. Imposible importar archivo de atajos. Deshacer Ventana Esta restaurando los accesos directos a sus valores predeterminados. 