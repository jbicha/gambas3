��    #      4  /   L        
   	               .  
   5     @     F     M     U     ]     e     m     t     �     �     �     �  (   �            	        %     +     8     ?     H     K     X  	   ^     h     q  6   w     �  ,   �  �   �     �     �     �                0     8     A     H     O     V     ]     d     ~     �     �     �  -   �       
        &     2     7     H     O     [     ^     p     |     �     �  3   �     �  .   �                       "       	                          
                                                           #                        !                  &1 toolbar &Close &Close current tab &Reset &Sort tabs &Undo Action Button1 Button2 Button3 Button4 Cancel Close &all other tabs Close current tab Configure &1 toolbar Configure main toolbar Configure shortcuts Do you really want to reset the toolbar? Find shortcut Go back Icon size Large Main toolbar Medium Next tab OK Previous tab Reset Separator Shortcut Small This shortcut is already used by the following action: Toolbars configuration You are going back to the default shortcuts. Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &1verktygsrad &Stäng &Stäng aktuell flik &Återställ &Sortera flikar &Ångra Åtgärd Knapp1 Knapp2 Knapp3 Knapp4 Avbryt Stäng &alla andra flikar Stäng aktuell flik Konfigurera &1 verktygsrad Konfigurera huvudverktygsrad Konfigurera genvägar Vill du verkligen återställa verktygsraden? Leta genväg Gå bakåt Ikonstorlek Stor Huvudverktygsrad Medium Nästa flik OK Föregående flik Återställ Skillnadstecken Genväg Liten Denne genväg används redan av följande åtgärd: Verktygsradskonfiguration Du kommer att ta tillbaka standardgenvägarna. 