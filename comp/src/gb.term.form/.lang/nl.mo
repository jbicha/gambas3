��          4      L       `      a   B  i   �   �     �  �  �                    Bonjour This components aim to give a set of tools to dialog with a VT102 terminal.

It provide a way to use common visual objects like windows, standart controls like label, checkbos, list and more... like in graphical mode.

It will have also a standalone class with common VT100 command to be used for more simple applications. Project-Id-Version: gb.term.form 3.10.90
PO-Revision-Date: 2017-08-28 18:40 UTC
Last-Translator: Willy Raets <gbWilly@openmailbox.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 - Doel van dit component is om een set instrumenten beschikbaar te stellen om de dialoog met een VT102 terminal aan te gaan.

Het voorziet in een manier om gebruikelijke visuele objecten zoals vensters, standaard controls zoals label, checkbox, lijsten en meer te gebruiken...zoals in grafische modus.

Het zal ook een standalone klasse bevatten met gebruikelijke VT100 comando's voor gebruik in simpelere applicaties. 