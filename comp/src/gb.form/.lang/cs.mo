��    N      �  k   �      �     �     �     �     �     �     �     �     �                     ,     5     =     N     P     ^     d     f  	   k     u     |     �     �     �     �     �     �  	   �  !   �  '      +   H     t     �     �     �     �     �     �  #   �     �     �     �  &   �     	  
   	  
   )	     4	     7	  	   P	     Z	     h	     n	     	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  	   
     
  >   
  &   Q
     x
     ~
     �
     �
  	   �
     �
     �
     �
     �     �     �     �     �     �     �               -     6     B     K     Y     j     l  	   �     �     �  	   �     �     �     �     �     �               )  	   :     D  #   V  (   z     �     �     �     �     �     �     �  5   �          0     9  1   J     |     �     �     �  $   �     �     �  	   �     �                    .  
   0     ;     C     E     W     q     �     �     �     �     �  =   �  *        /     4     A     E     ]     i     |           H         N   ;   K       8   5       A       D          M      .   0      	          6                    !       >      *   2       B   G   E       "   <      9   %         I               4   1                 =   J          -             #   F       $   &           
   +          7   ?   (   :   '              ,         /       C   )                            3   @   L               &1 B &1 GiB &1 KiB &1 MiB &1 properties &Bookmark current directory &Create directory &Delete &Edit bookmarks... &Next &Properties &Refresh &Rename &Uncompress file A All files (*) Apply B Bold Bookmarks Cancel Cannot create directory. Cannot rename directory. Cannot rename file. Cannot uncompress file. Close Delete file Desktop Directory Do not display this message again Do you really want to delete that file? Do you really want to remove this bookmark? Edit bookmarks Follow color grid G Group H Hidden Home How quickly daft jumping zebras vex Image preview Italic Last modified More controls for graphical components Name New folder Next month OK Open in &file manager... Overwrite Overwrite all Owner Parent directory Path Permissions Previous month R Relative Remove S Show &details Show &hidden files Show &image preview Show files Size Step #&1 Strikeout System The '/' character is forbidden inside file or directory names. This file or directory already exists. Today Transparent Type Unable to delete file. Underline Unknown archive. V Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 - - - - &1 vlastnosti Přidat adresář do &záložek Vy&tvořit adresář S&mazat Upravit &záložky... &Další &Vlastnosti &Obnovit Pře&jmenovat &Rozbalit soubor - Všechny soubory (*) Aplikovat - Tučné Záložky Zrušit Nelze vytvořit adresář. Nelze přejmenovat adresář. Nelze přejmonovat soubor. Nelze rozbalit soubor. Zavřít Smazat soubor Pracovní plocha Adresář Již nezobrazovat Opravdu chcete smazat tento soubor? Opravdu chcete odstranit tuto záložku? Upravit záložky Následuj barvu mřížky - Skupina - Skryté Domů Příliš žluťoučký kůň úpěl ďábelské ódy Náhled obrázku Kurzíva Poslední změna Další ovládací prvky pro grafické komponenty Název Nová složka Následující měsíc - Otevřít v &souborevém manageru... Přepsat Vše přepsat Vlastník Rodičovský adresář Cesta Oprávnění Předchozí měsíc - Relativní Odebrat - Zobrazit &detaily Zobrazit &skryté soubory Zobrazit &náhled obrázku Zobrazit soubory Velikost Krok #&1 Přeškrknuté Systém Uvnitř názvu souboru nebo adresáře je znak '/' zakázán. Tento soubor nebo adresář již existuje. Dnes Průhlednost Typ Soubor nelze odstranit. Podtržené Neznámí archív. - 