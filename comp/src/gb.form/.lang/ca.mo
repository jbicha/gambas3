��    6      �  I   |      �     �     �     �     �     �     �                      	        #     *     C     \     t     z  !   �  +   �     �     �     �     �     �  #   �          %  &   3     Z  
   _  
   j     u     x  	   �     �     �     �     �     �     �     �     �  
   �     �     �  	          >     &   R     y  	        �     �  -  �  -   �     �      	  	   ,	     6	     B	     [	     ]	     r	     t	     |	  	   �	     �	  '   �	  !   �	     
  
   
  #   
  3   6
     j
     �
     �
     �
     �
  ;   �
     �
     �
  )        /     3     @     M  !   U     w     �     �     �     �  	   �     �     �     �     �     �                 F      &   g     �  
   �     �     �               3                 '             6         (       /          +         &   !   2   ,   $          	   -                     #         5   .                 1       )                 0       4   %   "       *                   
                        &Bookmark current directory &Create directory &Edit bookmarks... &Next &Refresh &Uncompress file A All files (*) B Bold Bookmarks Cancel Cannot create directory. Cannot rename directory. Cannot uncompress file. Close Desktop Do not display this message again Do you really want to remove this bookmark? Edit bookmarks Follow color grid G H Home How quickly daft jumping zebras vex Italic Last modified More controls for graphical components Name New folder Next month OK Open in &file manager... Overwrite Overwrite all Path Previous month R Remove S Show &details Show &hidden files Show files Size Step #&1 Strikeout System The '/' character is forbidden inside file or directory names. This file or directory already exists. Today Underline Unknown archive. V Project-Id-Version: gb.form
PO-Revision-Date: 2011-03-20 15:34+0100
Last-Translator: Jordi Sayol <g.sayol@yahoo.es>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Catalan
 Crea adreça d'&interès del directori actual &Crea un directori &Edita les adreces d'interès... &Següent &Actualitza &Descomprimeix el fitxer - Tots els fitxers (*) - Negreta Adreces d'interès Canceŀla No es pot crear el directori. No es pot canviar el nom del directori. No es pot descomprimir el fitxer. Tanca Escriptori No tornis a mostrar aquest missatge Segur que voleu esborrar aquest adreça d'interès? Edita les adreces d'interès Seguiu la graella de color - - Inici Jove xef, porti whisky amb quinze glaçons d'hidrogen, coi! Cursiva Última modificació Més controls per als components gràfics Nom Nova carpeta Mes següent D'acord Obre amb el gestor de &fitxers... Sobreescriure Sobreescriure-ho tot Camí Mes anterior - Suprimeix - Mostra els &detalls Mostra els fitxers &ocults Mostra els fitxers Mida Pas número &1 Barrat Sistema El caràcter '/' no està permès dins el nom de fitxers o directoris. Aquest fitxer o directori ja existeix. Avui Subratllat Fitxer desconegut. - 