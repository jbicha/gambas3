��          �      �       0     1     6  0   >     o  ,   s  .   �  &   �  &   �          "     (     .     3     4     ;  4   C     x  :   |  >   �  7   �  9   .     h     n     v     ~     
   	                                                  Fast Fractal Mandelbrot Fractal with Just-In-Time compilation Max Press F to activate Just-In-Time compilation Press F to deactivate Just-In-Time compilation Press R to hide rectangle optimization Press R to show rectangle optimization Slow Speed Tasks Zoom Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Rapide Fractal Fractale de Mandelbort avec compilation Just-In-Time Max Appuyez sur F pour activer la compilation "juste à-temps" Appuyez sur F pour désactiver la compilation "juste à-temps" Appuyez sur R pour cacher l'optimisation par rectangles Appuyez sur R pour afficher l'optimisation par rectangles Lente Vitesse Tâches Zoom 