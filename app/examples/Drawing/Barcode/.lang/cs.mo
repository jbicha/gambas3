��          �            x     y     �  �   �     Y     b     j     {     �  +   �     �     �     �     �  I   �     )     /     0     H  �   `     <     J     Y  	   q     {  5   �     �     �     �     �  F   �     @                      	                                    
                     &Print Barcode &Refresh Barcode A module to print EAN-13 barcodes. The barcode is constructed from first principles (http://en.wikipedia.org/wiki/European_Article_Number). The barcode can be formatted for screen or printer. About... Barcode Barcode Printing E&xit Height Please enter a valid 13 digit EAN-13 number PosX PosY Size for printer Size for screen This program was made by Charles Guerin and
modified by Benoît Minisini. Width Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &Tisk čárového kódu &Obnovit čárový kód Modul pro tisk čárových kódů EAN-13. Čárového kódu je postaven z prvních principů (http://en.wikipedia.org/wiki/European_Article_Number). Čárový kód může být formátovány pro obrazovku nebo tiskárnu. O programu... Čárový kód Tisk čárových kódů &Ukončit Výška Zadejte prosím platné 13-ti místné číslo EAN-13 Pozice X Pozice Y Velikost pro tiskárnu Velikost pro obrazovku Tento program byl vytvořil Charles Guerin a
upravil Benoît Minisini. Šířka 