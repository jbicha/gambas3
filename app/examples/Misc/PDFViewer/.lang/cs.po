#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "PdfViewer"
msgstr "-"

#: .project:2
msgid "A simple PDF Viewer, used as example of gb.pdf capabilities"
msgstr "Ukázka PDF prohlížeče, příklad užití schopnosti gb.pdf"

#: FMain.class:52
msgid "Pdf documents"
msgstr "PDF dokument"

#: FMain.form:30
msgid "Simple PDF document viewer"
msgstr "Ukázka prohížeče PDF dokumentů"

#: FMain.form:38
msgid "Open file..."
msgstr "Otevřít soubor..."

#: FMain.form:44
msgid "About..."
msgstr "O aplikaci..."

#: FMain.form:51
msgid "Rotate"
msgstr "Rotace"

#: FMain.form:58
msgid "Zoom out"
msgstr "Oddálit"

#: FMain.form:65
msgid "Zoom in"
msgstr "Přiblížit"

#: FMain.form:76
msgid "Find text"
msgstr "Najdi text"

#: FMain.form:90
msgid "Previous page"
msgstr "Předchozí stránka"

#: FMain.form:99
msgid "Next Found"
msgstr "Další nalezeno"

#: FMain.form:119
msgid "Page"
msgstr "Stránka"

#: FMain.form:130
msgid "Go to this page"
msgstr "Jdi na tuto stránku"

#: FMain.form:143
msgid "Next page"
msgstr "Další stránka"

#: Fabout.form:20
msgid "<h2>Simple PDF document viewer</h2><p>Gambas example by Daniel Campos Fernández  and Bernd Brinkmann"
msgstr "<h2>Jednotuchý prohlížeč PDF dokumentů</h2><p>Gambas příklad od Daniel Campos Fernández a Bernd Brinkmann"

#: Fabout.form:34
msgid "OK"
msgstr "-"
