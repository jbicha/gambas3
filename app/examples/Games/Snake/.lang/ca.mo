��          �      �        !   	     +  "   2     U     [     a     j     p     }     �     �     �     �  )   �     �     �     �  
   �            D   $  $   i  +  �  $   �     �  .   �          #     (     /     6     D     I     P  
   V     a  6   |     �     �     �     �  
   �     �  H     0   a                                                           
       	                                      
Press Game -> New to play again. &About &Auto Advance Speed (Challenge ME) &Cool &Game &GrandMa &Help &How To Play &New &Pause &Quit &Speed &Speed Freak Do not Hit the walls, or bite yourself!

 Eat as many apples as you can
 Score:  Snake Snake game Speed:  The Rules are simple:
 The snake's soul will always be remembered.
Game Over!!
You scored:  Use the arrow keys to move the snake Project-Id-Version: Snake
PO-Revision-Date: 2011-03-20 22:35+0100
Last-Translator: Jordi Sayol <g.sayol@yahoo.es>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Catalan
 
Prem Joc -> Nou per tornar a jugar. Qu&ant a... &Increment automàtic de velocitat (Desafia'm) &Moderat &Joc À&via A&juda Com &jugar-hi &Nou &Pausa &Surt &Velocitat &Velocitat fora de control No colpegis les parets, ni et mosseguis a tu mateix!

 Mengeu tantes pomes com podeu
 Puntuació: Serp Joc de la serp Velocitat: Les normes son senzilles:
 Sempre recordarem l'ànima de la serp.
Joc acabat!!
La teva puntuació:  Utilitzeu les tecles de fletxa per moure la serp 